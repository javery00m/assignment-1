<!--<div class="customers view">-->


<h2><?php echo __('Customer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Streetaddress'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['streetaddress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Primaryemail'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['primaryemail']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Homephone'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['homephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cellphone'); ?></dt>
		<dd>
			<?php echo h($customer['Customer']['cellphone']); ?>
			&nbsp;
		</dd>
	</dl>
<!--<Bitbucket test>-->
<!--</div>-->
<div class="related">
	<h2><?php echo __('Stocks'); ?></h2>

	<?php if (!empty($customer['Stocks'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>

		<th><?php echo __('Stock Symbol'); ?></th>
		<th><?php echo __('No. Shares'); ?></th>
		<th><?php echo __('Purchase Price'); ?></th>
		<th><?php echo __('Date of Purchase'); ?></th>
		<th><?php echo __('Original Value'); ?></th>
		<th><?php echo __('Current Price'); ?></th>
		<th><?php echo __('Stock Increase/Decrease'); ?></th>
		<th><?php echo __('Current Value'); ?></th>
		<th><?php echo __('Gains/Losses'); ?></th>
		<th><?php echo __('Percentage Gain/Loss'); ?></th>
	</tr>
	<?php foreach ($customer['Stocks'] as $stocks): ?>
		<tr>

			<td><?php echo $stocks['stsymbol']; ?></td>
			<td><?php echo $stocks['noshares']; ?></td>
			<td><?php echo '$'.$stocks['purchaseprice']; ?></td>
			<td><?php echo $stocks['datepurchased']; ?></td>
			<td><?php echo '$'.$stocks['noshares']*$stocks['purchaseprice']; ?></td>
			<td>$<?php
				require_once('nusoap.php');

			$c = new nusoap_client('http://loki.ist.unomaha.edu/~groyce/ws/stockquoteservice.php');

				echo $c->call('getStockQuote' ,
				array('symbol' => $stocks['stsymbol']));?></td>
			<td>$<?php echo $c->call('getStockQuote',
						array('symbol' => $stocks['stsymbol']))-$stocks['purchaseprice'] ?></td>
			<td>$<?php echo $stocks['noshares']*$c->call('getStockQuote',
						array('symbol' => $stocks['stsymbol'])) ?></td>
			<td>$<?php echo $stocks['noshares']*$c->call('getStockQuote',
						array('symbol' => $stocks['stsymbol']))-$stocks['noshares']*$stocks['purchaseprice'] ?></td>
			<td><?php echo round((($stocks['noshares']*$c->call('getStockQuote',
						array('symbol' => $stocks['stsymbol'])))/($stocks['noshares']*$stocks['purchaseprice']))*100,2) ?>%</td>
		</tr>
	<?php endforeach; ?>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><?php $tot_original= 0;
				foreach ($customer['Stocks'] as $stocks)
				{ $tot_original += $stocks['noshares']*$stocks['purchaseprice'];}?><b>$<?php echo $tot_original;?></b></td>
			<td></td>
			<td></td>
			<td><?php $tot_current= 0;
				foreach ($customer['Stocks'] as $stocks)
				{ $tot_current += $stocks['noshares']*$c->call('getStockQuote',
						array('symbol' => $stocks['stsymbol']));}?><b>$<?php echo $tot_current;?></b></td>
			<td><?php $tot_gain= 0;
				foreach ($customer['Stocks'] as $stocks)
				{ $tot_gain += $stocks['noshares']*$c->call('getStockQuote',
						array('symbol' => $stocks['stsymbol']))-$stocks['noshares']*$stocks['purchaseprice'];}?><b>$<?php echo $tot_gain;?></b></td>
			<td></td>
			</tr>
	</table>
	<?php endif; ?>


</div>
<div class="related">
	<h2><?php echo __('Investments'); ?></h2>
	<?php if (!empty($customer['Investments'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Category'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Acquired Value'); ?></th>
		<th><?php echo __('Acquired Date'); ?></th>
		<th><?php echo __('Recent Value'); ?></th>
		<th><?php echo __('Recent Date'); ?></th>
	</tr>
	<?php foreach ($customer['Investments'] as $investments): ?>
		<tr>
			<td><?php echo $investments['category']; ?></td>
			<td><?php echo $investments['description']; ?></td>
			<td><?php echo '$'.$investments['acquiredvalue']; ?></td>
			<td><?php echo $investments['acquireddate']; ?></td>
			<td><?php echo '$'.$investments['recentvalue']; ?></td>
			<td><?php echo $investments['recentdate']; ?></td>

		</tr>
	<?php endforeach; ?>
<tr>
	<td></td>
	<td></td>
	<td><?php $tot_acquired= 0;
			foreach ($customer['Investments'] as $investments)
			{ $tot_acquired += $investments['acquiredvalue'];}?><b>$<?php echo $tot_acquired;?></b></td>
	<td></td>
	<td><?php $tot_recent= 0;
		foreach ($customer['Investments'] as $investments)
		{ $tot_recent += $investments['recentvalue'];}?><b>$<?php echo $tot_recent;?></b></td>

	</td>
<td></td>
</tr>

	</table>
<?php endif; ?>
</div>

<div class="related">
	<h2><?php echo __('Total Portfolio Value'); ?></h2>
	<?php if (!empty($customer['Investments'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Category'); ?></th>
				<th><?php echo __('Original Value'); ?></th>
				<th><?php echo __('Current Value'); ?></th>
			</tr>
				<tr>
					<td><?php echo 'Stocks'; ?></td>
					<td><?php echo '$'.$tot_original; ?></td>
					<td><?php echo '$'.$tot_current; ?></td>
				</tr>
			<tr>
				<td><?php echo 'Investments'; ?></td>
				<td><?php echo '$'.$tot_acquired; ?></td>
				<td><?php echo '$'.$tot_recent; ?></td>
			</tr>
			<tr>
				<td><b>Total Portfolio Value</b></td>
				<td><b>$</b><b><?php echo $tot_original+$tot_acquired?></b></td>
				<td><b>$</b><b><?php echo $tot_current+$tot_recent?></b></td>
			</tr>
		</table>
	<?php endif; ?>
</div>



